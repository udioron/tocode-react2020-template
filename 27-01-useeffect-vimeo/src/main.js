import React from 'react'
import ReactDOM from 'react-dom'

import './main.css'
import { App } from './App'

const root = document.querySelector('main')
ReactDOM.render(<App/>, root)
